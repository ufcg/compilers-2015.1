# compilers-20151

Uniersidade Federal de Campina Grande - UFCG

Departamento de Sistemas e Computação - DSC

Module: Compilers

Year: 2015.1

Students:

* Guilherme Steinmuller Pimentel
* Heitor Meira de Melo
* Thalita Gonçalves

Professor:

* Dr. Franklin Ramalho

This is the design of a Java compiler (Lexical Analysis, Syntax Analysis and Code Generation) using JFlex and CUP.

The suggested grammar can be found [here](http://cui.unige.ch/isi/bnf/JAVA/BNFindex.html)

# Preparing environment

We use [Eclipse - Luna 4.4](https://www.eclipse.org) with [CUP](http://www2.cs.tum.edu/projects/cup/eclipse.php) plugin.

To import the project to Eclipse:

Clone the repository to your workspace:

<code>
git clone git@github.com:htrmeira/compilers-2015.1.git
</code>

Then, on Eclipse:

**File** -> **New** -> **Other** -> **Cup** -> **Cup Java Project**

On **Project name**, enter the same name of the destination folder of the git clone. In this example, compilers-2015.1.

Then, **Click Next**.
Then, **Click Next** again.

In the next page, uncheck **Create calculator example files** and **Use jflex**.

If an error window appear, just **Click Cancel**.

This might have created a package **com.example**, just delete it. (Please, I am beggin you. Do it.)

# Running (in Eclipse)

To run you need to generate the java classes. This classes are generated from the .jflex and .cup files, so every time you make a change in these files you will need to run build.xml

To do it in Eclipse, right click the file build.xml and: **Run As** -> **Ant Build**

Then, execute the project as a Java Application, the class **br.edu.ufcg.dsc.compilers.driver.Driver**.

You need to configure Eclipse to pass an argumento to the program with the path of the file you are compiling.


# Running (building with ant)
In order to compile this compiler you will need to have ant installed.

Inside the root directory of the project (compilers-2015.1/), execute:

<code>
user@azazel:~/compilers-2015.1$ ant
</code>

It will create the dist/ directory, inside it you will have the directory lib/ (with the .jars necessary to run the compiler) and a executable named 'compiler'.

To run it, execute:

<code>
user@azazel:~/compilers-2015.1/dist$ ./compiler FILE_PATH
</code>

There is also a script named 'run-tests.sh' the runs the compiler with the files in the directory 'compilers-2015.1/input-tests' that has the a name starting with 'input-'.
