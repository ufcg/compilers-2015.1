/*
 This parser parses java code.

 There is some code in this parser, that will be used on the next phase of the compiler.
 (e.g. some terminal types are declared).

 We also, made some changes related to the proposed grammar,
 but only to make it more like the actual java.
 @authors Guilherme Steinmuller Pimentel, Heitor Meira de Melo, Thalita Gonçalves. 
*/

package br.edu.ufcg.dsc.compilers.gen;

import java_cup.runtime.*;
import br.edu.ufcg.dsc.compilers.gen.Lexer;
import br.edu.ufcg.dsc.compilers.Params;

parser code {:
  protected Lexer lexer;
:}

/* define how to connect to the scanner! */
init with {:
  ComplexSymbolFactory f = new ComplexSymbolFactory();
  symbolFactory = f;
  lexer = new Lexer(f, Params.getSrcFile());
:};

scan with {: return lexer.next_token(); :};

/* reserved words */
terminal					PUBLIC, PRIVATE, PROTECTED, STATIC, FINAL, NATIVE, SYNCHRONIZED, ABSTRACT,
							BOOLEAN, BYTE, CHAR, SHORT, INT, FLOAT, LONG, DOUBLE, QUESTION, COLON, 
							RETURN, THROW, BREAK, CONTINUE, SUPER, THIS, NEW, INSTANCEOF, TRY, CATCH,
							FINALLY, PACKAGE, IMPORT, VOID;

/* loops */
terminal 					FOR, DO, WHILE;

/* conditionals */
terminal					IF, ELSE, SWITCH, CASE, DEFAULT;

/* null */
terminal					NULL, TRUE, FALSE, CHARACTER_LITERAL, STRING_LITERAL;
terminal java.lang.Number	INTEGER_LITERAL, HEX_LITERAL, OCT_LITERAL, FLOAT_LITERAL;

/* numeric expression */
terminal					EQ, MINUS, PLUS, MULT, DIVISION, MOD, INCREMENT, DECREMENT, PLUS_EQ, 
							MINUS_EQ, MULT_EQ, DIVISION_EQ, MOD_EQ;

/* boolean operators */
terminal					GT, LT, EQEQ, LTEQ, GTEQ, NOTEQ, AND_AND, OR_OR;

/* logical operators */
terminal					NOT, AND, OR, XOR, OR_EQ, XOR_EQ, OR_OR_EQ, AND_EQ;

/* bit operators */
terminal					TILDE, LSHIFT, RSHIFT, RSHIFT_EQ, U_RSHIFT;

/* general terminals */
terminal					LBRACKET, RBRACKET, COMMA, DOT, LSQUARE_BRACKET, RSQUARE_BRACKET,
							LPARENTHESIS, RPARENTHESIS, SEMICOLON;
							
terminal					CLASS, EXTENDS, IMPLEMENTS, INTERFACE;
terminal java.lang.String	IDENTIFIER;

/* start symbol */
non terminal		program;

non terminal		field_declaration, field_declarations_opt, field_declarations, variable_declaration, 
					variable_declarator, variable_initializer, static_initializer, declarators, method_body,
					variable_initializer_list, variable_initializer_list_opt, comma_opt, attribution_initializer_opt,
					variable_declarator_list, constructor_declaration, expression, expression_list, expression_opt,
					numeric_expression, testing_expression, logical_expression, type, type_specifier, arglist_opt,
					arglist, class_or_interface_type, class_or_interface_types, modifier, modifiers, modifiers_opt, 
					bit_expression, casting_expression, creating_expression, creation_expressions, literal_expression,  
					integer_literal, assignment_logical_operators, binary_logical_operators,
					primitive_type, method_declaration, parameter, parameter_list, parameter_list_opt,
					minus_increment_decrement, increment_decrement, assignment_operator, binary_operator, operator,
					boolean_operators, logical_operators, bit_operators, array_expression_opt, statement, statement_block, 
					statement_block_opt, statement_list_opt, statement_list, class_declaration, interface_declaration,
					semicolon_opt, array_declaration, array_declarations, array_declarations_opt, identifier_opt,
					compilation_unit, package_statement, package_statement_opt, import_statement, import_names,
					import_statements_opt, import_statements, type_declaration, type_declarations, type_declarations_opt,
					inheritance_opt, interfaces_opt, multi_inheritance_opt, attribution_statement, simple_name, 
					expression_right_side, import_name, if_statement, else_opt, do_statement, while_statement,
					for_statement, for_statement_options, try_statement, switch_statement, switch_body, switch_bodys,
					switch_bodys_opt, finally_opt, catch_statement, catch_statements, catch_statements_opt;

precedence left BOOLEAN, BYTE, CHAR, SHORT, INT, FLOAT, LONG, DOUBLE, VOID;
precedence left IF, ELSE, TRY, CATCH, FINALLY;
precedence left OR_OR_EQ;
precedence left GT, LT, GTEQ, LTEQ, EQEQ, NOTEQ;
precedence left PLUS_EQ, MINUS_EQ, MULT_EQ, DIVISION_EQ, MOD_EQ;
precedence left AND_EQ, OR_EQ, XOR_EQ, AND_AND, OR_OR;
precedence left INCREMENT, DECREMENT;
precedence left AND, OR, XOR, NOT;
precedence left PLUS, MINUS, MULT, DIVISION, MOD;
precedence left QUESTION, COLON, COMMA, DOT;
precedence left RSHIFT_EQ, LSHIFT, RSHIFT, U_RSHIFT;
precedence left LBRACKET, RBRACKET;
precedence left LSQUARE_BRACKET, RSQUARE_BRACKET;
precedence left LPARENTHESIS, RPARENTHESIS;
precedence left EQ;
precedence left INSTANCEOF;
precedence left IDENTIFIER;

start with program;

program ::= compilation_unit;

/*
 http://cui.unige.ch/isi/bnf/JAVA/compilation_unit.html
*/
compilation_unit ::= package_statement_opt import_statements_opt type_declarations_opt;

package_statement_opt ::= package_statement |; 

import_statements_opt ::= import_statements |;

type_declarations_opt ::= type_declarations |;

/*
 http://cui.unige.ch/isi/bnf/JAVA/package_statement.html
*/
package_statement ::= PACKAGE class_or_interface_type SEMICOLON;

import_statements ::= import_statements import_statement
					| import_statement;

import_name ::= import_name DOT IDENTIFIER | IDENTIFIER ;

/*
 The grammar of import_statement assumes a obligatory double
 semicolon at the end of a general import statement (the one
 that finishes with .*). That does not make sense, so we are
 removing in it. 
*/
import_names ::= import_name DOT MULT | import_name ;

/*
 http://cui.unige.ch/isi/bnf/JAVA/import_statement.html
 There is a difference from the one declared in grammar,
 see import_names.
*/
import_statement ::= IMPORT import_names SEMICOLON;

type_declarations ::= type_declaration
					| type_declarations type_declaration;

semicolon_opt ::= SEMICOLON |;

/*
 http://cui.unige.ch/isi/bnf/JAVA/type_declaration.html
 
 Note that doc_comment is being treated by the lexer.
 The grammar show a SEMICOLON at the end of a type_declaration,
 we chose to let it be optional.
*/
type_declaration ::= class_declaration semicolon_opt
					| interface_declaration semicolon_opt;

inheritance_opt ::= EXTENDS class_or_interface_type |;

multi_inheritance_opt ::= EXTENDS class_or_interface_types |; 

interfaces_opt ::= IMPLEMENTS class_or_interface_types |;

/*
 http://cui.unige.ch/isi/bnf/JAVA/class_declaration.html
*/
class_declaration ::= modifiers_opt CLASS IDENTIFIER
					inheritance_opt interfaces_opt 
					LBRACKET field_declarations_opt RBRACKET;

/*
 http://cui.unige.ch/isi/bnf/JAVA/interface_declaration.html
*/
interface_declaration ::= modifiers_opt INTERFACE IDENTIFIER
						multi_inheritance_opt LBRACKET field_declarations_opt RBRACKET;

/*
 http://cui.unige.ch/isi/bnf/JAVA/static_initializer.html
*/
static_initializer ::= STATIC statement_block;

field_declarations_opt ::= field_declarations |;

field_declarations ::= field_declarations field_declaration
					| field_declaration;

/*
 http://cui.unige.ch/isi/bnf/JAVA/constructor_declaration.html
*/
constructor_declaration ::= modifiers_opt IDENTIFIER
						LPARENTHESIS parameter_list_opt RPARENTHESIS
						statement_block_opt;

method_body ::= statement_block | SEMICOLON;

/*
 http://cui.unige.ch/isi/bnf/JAVA/method_declaration.html
*/
method_declaration ::= modifiers_opt type IDENTIFIER:i
					LPARENTHESIS parameter_list_opt RPARENTHESIS
					array_declarations_opt method_body;

declarators ::= method_declaration
			| constructor_declaration
			| variable_declaration;

/*
 http://cui.unige.ch/isi/bnf/JAVA/field_declaration.html
 Note that doc_comment is being treated by the lexer.
*/
field_declaration ::= declarators
					| static_initializer
					| SEMICOLON;

/*
 http://cui.unige.ch/isi/bnf/JAVA/modifier.html
*/
modifier ::= PUBLIC
			| PRIVATE
			| PROTECTED
			| STATIC
			| FINAL
			| NATIVE
			| SYNCHRONIZED
			| ABSTRACT;

simple_name ::= IDENTIFIER;

/*
 class_name, interface_name and package_name rules are the same.

 http://cui.unige.ch/isi/bnf/JAVA/class_name.html
 http://cui.unige.ch/isi/bnf/JAVA/interface_name.html
 http://cui.unige.ch/isi/bnf/JAVA/package_name.html
 */
 class_or_interface_type ::= simple_name DOT class_or_interface_type 
 							| simple_name;

class_or_interface_types ::= class_or_interface_type COMMA class_or_interface_types
						| class_or_interface_type; 

primitive_type ::= BOOLEAN
				| BYTE
				| CHAR
				| SHORT
				| INT
				| FLOAT
				| LONG
				| DOUBLE
				| VOID;

/*
 http://cui.unige.ch/isi/bnf/JAVA/type_specifier.html
*/
type_specifier ::= primitive_type
				| class_or_interface_type;

array_declaration ::= LSQUARE_BRACKET RSQUARE_BRACKET; 

array_declarations ::= array_declarations array_declaration
					| array_declaration;

array_declarations_opt ::= array_declarations |;

/*
  http://cui.unige.ch/isi/bnf/JAVA/type.html
*/
type ::= primitive_type array_declarations_opt
	| class_or_interface_type array_declarations_opt;

increment_decrement ::= INCREMENT
					| DECREMENT;

minus_increment_decrement ::= MINUS
					| increment_decrement;

assignment_operator ::= PLUS_EQ
					| MINUS_EQ
					| MULT_EQ
					| DIVISION_EQ
					| MOD_EQ;

binary_operator ::= PLUS
				| MINUS
				| MULT
				| DIVISION
				| MOD;

operator ::= assignment_operator
			| binary_operator;

/*
 http://cui.unige.ch/isi/bnf/JAVA/numeric_expression.html

 Note that string_expression (http://cui.unige.ch/isi/bnf/JAVA/string_expression.html),
 sintax is included in numeric_expression, and for this reason we are
 not declaring it, since declaring it again would result in unecessary
 reduce-reduce conflict.
*/
numeric_expression ::= minus_increment_decrement expression
					| expression increment_decrement
					| expression operator expression;

boolean_operators ::= GT
					| LT
					| GTEQ
					| LTEQ
					| EQEQ
					| NOTEQ;
/*
 http://cui.unige.ch/isi/bnf/JAVA/testing_expression.html
*/
testing_expression ::= expression boolean_operators expression;

assignment_logical_operators ::= AND_EQ
							| OR_EQ
							| XOR_EQ
							| OR_OR_EQ;

/*
 The grammar does not have OR_OR, we are adding to make
 it more like the actual java, facilitating tests.
*/
binary_logical_operators ::= AND
							| OR
							| XOR
							| AND_AND
							| OR_OR;

logical_operators ::= assignment_logical_operators
					| binary_logical_operators;
/*
 http://cui.unige.ch/isi/bnf/JAVA/logical_expression.html
*/
logical_expression ::= NOT expression
					| expression logical_operators expression
					| expression QUESTION expression COLON expression
					| TRUE
					| FALSE;

bit_operators ::= RSHIFT_EQ
				| LSHIFT
				| RSHIFT
				| U_RSHIFT;

/*
 http://cui.unige.ch/isi/bnf/JAVA/bit_expression.html
*/
bit_expression ::= TILDE expression
				| expression bit_operators expression;

/*
 http://cui.unige.ch/isi/bnf/JAVA/arglist.html
*/
arglist ::= expression
		| arglist COMMA expression;
		
arglist_opt ::= arglist |;

array_expression_opt ::= LSQUARE_BRACKET expression RSQUARE_BRACKET |;

creation_expressions ::= class_or_interface_type LPARENTHESIS arglist_opt RPARENTHESIS
					| type_specifier array_expression_opt array_declarations_opt
					| LPARENTHESIS expression RPARENTHESIS;

/*
 @htrmeira: new "(" expression ")"
 Does not seems to be a valid java sentence, but it is specified in grammar:
 http://cui.unige.ch/isi/bnf/JAVA/creating_expression.html 
*/
creating_expression ::= NEW creation_expressions; 

/*
 http://cui.unige.ch/isi/bnf/JAVA/integer_literal.html
*/
integer_literal ::=  INTEGER_LITERAL
				| HEX_LITERAL
				| OCT_LITERAL;

/*
 http://cui.unige.ch/isi/bnf/JAVA/literal_expression.html
*/
literal_expression ::= integer_literal
					| FLOAT_LITERAL
					| STRING_LITERAL
					| CHARACTER_LITERAL;

modifiers ::= modifiers modifier
			| modifier;

modifiers_opt ::= modifiers |;

variable_initializer_list ::= variable_initializer_list COMMA variable_initializer
							| variable_initializer;

comma_opt ::= COMMA |;

variable_initializer_list_opt ::=  variable_initializer_list comma_opt |;

/*
 http://cui.unige.ch/isi/bnf/JAVA/variable_initializer.html
*/
variable_initializer ::= expression
					| LBRACKET variable_initializer_list_opt RBRACKET;

attribution_initializer_opt ::= attribution_statement |;

/*
 http://cui.unige.ch/isi/bnf/JAVA/variable_declarator.html
*/
variable_declarator ::= IDENTIFIER array_declarations_opt attribution_initializer_opt;

variable_declarator_list ::= variable_declarator
						| variable_declarator COMMA variable_declarator_list;

/*
 http://cui.unige.ch/isi/bnf/JAVA/variable_declaration.html
*/
variable_declaration ::= modifiers_opt type variable_declarator_list SEMICOLON
					| type variable_declarator_list SEMICOLON;

attribution_statement ::= EQ variable_initializer;

/*
 http://cui.unige.ch/isi/bnf/JAVA/casting_expression.html
 Note that we are adding the optionals for array types to the grammar. 
*/
casting_expression ::= LPARENTHESIS primitive_type RPARENTHESIS expression
				| LPARENTHESIS primitive_type array_declarations RPARENTHESIS expression
				| LPARENTHESIS class_or_interface_type RPARENTHESIS expression
				| LPARENTHESIS class_or_interface_type array_declarations RPARENTHESIS expression;

/*
 http://cui.unige.ch/isi/bnf/JAVA/expression.html
*/
expression_right_side ::= LPARENTHESIS arglist_opt RPARENTHESIS expression_right_side
			| DOT expression expression_right_side
			| LSQUARE_BRACKET expression RSQUARE_BRACKET expression_right_side
			| COMMA expression expression_right_side
			| INSTANCEOF class_or_interface_type expression_right_side
			| attribution_statement
			|;

expression ::= numeric_expression expression_right_side
			| testing_expression expression_right_side
			| logical_expression expression_right_side
			| bit_expression expression_right_side
			| casting_expression expression_right_side
			| creating_expression expression_right_side
			| literal_expression expression_right_side
			| NULL expression_right_side
			| SUPER expression_right_side
			| THIS expression_right_side
			/*
			 We are making an identifier more general,
			 this is replacing the "| IDENTIFIER" rule.
			*/
			| class_or_interface_type expression_right_side
			| LPARENTHESIS expression RPARENTHESIS expression_right_side;

/*
 http://cui.unige.ch/isi/bnf/JAVA/parameter.html
*/
parameter ::= type IDENTIFIER array_declarations_opt;

/*
 http://cui.unige.ch/isi/bnf/JAVA/parameter_list.html
*/
parameter_list ::= parameter_list COMMA parameter
				| parameter;

parameter_list_opt ::= parameter_list |;

/*
 http://cui.unige.ch/isi/bnf/JAVA/statement.html
*/
statement ::= variable_declaration 
			| expression SEMICOLON 
			| statement_block
			| if_statement
			| do_statement
			| while_statement
			| for_statement
			| try_statement
			| switch_statement
			| SYNCHRONIZED LPARENTHESIS expression RPARENTHESIS statement
			| RETURN expression_opt SEMICOLON 
			| THROW expression SEMICOLON
			| IDENTIFIER COLON statement
			| BREAK identifier_opt  SEMICOLON 
			| CONTINUE identifier_opt  SEMICOLON 
			| SEMICOLON;

statement_list ::= statement_list statement
				| statement;

statement_list_opt ::= statement_list |;

/*
 http://cui.unige.ch/isi/bnf/JAVA/statement_block.html
*/
statement_block ::= LBRACKET statement_list_opt RBRACKET;

statement_block_opt ::= statement_block | ;

identifier_opt ::= IDENTIFIER |;

expression_opt ::= expression |;

/*
 http://cui.unige.ch/isi/bnf/JAVA/if_statement.html
 Note that we are declaring the precedence for ELSE
 (in favor of shifting), so we do not have ambiguity
 here or in cases like this.
*/
if_statement ::= IF LPARENTHESIS expression RPARENTHESIS statement else_opt;

else_opt ::= ELSE statement |;

/* 
 http://cui.unige.ch/isi/bnf/JAVA/do_statement.html
*/
do_statement ::= DO statement WHILE LPARENTHESIS expression RPARENTHESIS SEMICOLON;

/*
 http://cui.unige.ch/isi/bnf/JAVA/while_statement.html
*/
while_statement ::= WHILE LPARENTHESIS expression RPARENTHESIS statement;

expression_list ::= expression_list COMMA expression | expression;

/*
 http://cui.unige.ch/isi/bnf/JAVA/for_statement.html
 Note that this for has a SEMICOLON in the end of the
 third expression, difering from normal Java.
*/
for_statement ::= FOR LPARENTHESIS 
				for_statement_options
				expression_opt SEMICOLON
				expression_opt RPARENTHESIS statement;

for_statement_options ::= variable_declaration
						| expression_list SEMICOLON
						| SEMICOLON ;

catch_statement ::= CATCH LPARENTHESIS parameter RPARENTHESIS statement;

catch_statements ::= catch_statements catch_statement
				| catch_statement;
catch_statements_opt ::= catch_statements |;

/*
 http://cui.unige.ch/isi/bnf/JAVA/try_statement.html
*/
try_statement ::= TRY statement catch_statements_opt finally_opt;

finally_opt ::= FINALLY statement |;

switch_body ::= CASE expression COLON
			| DEFAULT COLON
			| statement;

switch_bodys ::= switch_bodys switch_body
			| switch_body;

switch_bodys_opt ::= switch_bodys |; 

/*
 http://cui.unige.ch/isi/bnf/JAVA/switch_statement.html
*/
switch_statement ::= SWITCH LPARENTHESIS expression RPARENTHESIS
					LBRACKET switch_bodys_opt RBRACKET;