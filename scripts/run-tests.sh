#!/bin/bash

INPUT_TEST_DIR=../input-tests

for i in `ls $INPUT_TEST_DIR/input-*`;do
	echo -n "compiling file: $i -> "
	./compiler $INPUT_TEST_DIR/$i;
done
