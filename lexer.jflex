package br.edu.ufcg.dsc.compilers.gen;

import java_cup.runtime.ComplexSymbolFactory;
import java_cup.runtime.ComplexSymbolFactory.Location;
import java_cup.runtime.Symbol;
import java.lang.*;
import java.io.InputStreamReader;

/*
 This lexer analyses java code.

 Some java code in this class is here thinking about next phase of the compiler.

 @authors Guilherme Steinmuller Pimentel, Heitor Meira de Melo, Thalita Gonçalves.
*/

%%

%class Lexer
%implements sym
%public
%unicode
%line
%column
%cup
%char
%cupdebug

%{
	StringBuffer string = new StringBuffer();

    public Lexer(ComplexSymbolFactory sf, java.io.InputStream is){
		this(is);
        symbolFactory = sf;
    }
	public Lexer(ComplexSymbolFactory sf, java.io.Reader reader){
		this(reader);
        symbolFactory = sf;
    }
    
    private StringBuffer sb;
    private ComplexSymbolFactory symbolFactory;
    private int csline, cscolumn;
    
    protected void emit_warning(String message){
    	System.out.println("scanner warning: " + message + " at : 2 "+ 
    			(yyline + 1) + " " + (yycolumn + 1) + " " + yychar);
    }
    
    protected void emit_error(String message){
    	System.out.println("scanner error: " + message + " at : 2" + 
    			(yyline + 1) + " " + (yycolumn + 1) + " " + yychar);
    }
    
	private static long parseToLong(int start, int end, String word, int radix) {
		long result = 0;

		for (int i = start; i < end; i++) { 
			result *= radix;
			result += Character.digit(word.charAt(i),radix);
		}
		return result;
	}
%}

Newline = \r | \n | \r\n
Whitespace = [ \t\f] | {Newline}

/* numbers literals (thanks moy) */
OctDigit = [0-7]
OctIntegerLiteral = 0+ [1-3]? {OctDigit} {1,15}

HexDigit = [0-9a-fA-F]
HexIntegerLiteral = 0 [xX] 0* {HexDigit} {1,8}

IntegerLiteral = (0 | [1-9][0-9]*)

ExponentPart = [eE] [+-]? {IntegerLiteral}

FloatLiteral = {IntegerLiteral} \. {IntegerLiteral}* {ExponentPart} {0,1} [fFdD] {0,1} | 
			(\. {IntegerLiteral} {ExponentPart} {0,1} [fFdD]) {0,1} |
			({IntegerLiteral} {ExponentPart} {0,1} [fFdD] {0,1})

EndOfLineComment = "//" [^\r\n]* {Newline}
CommentContent = ( [^*] | \*+[^*/] )*
TraditionalComment = "/*" {CommentContent} \*+ "/"
Comment = {TraditionalComment} | {EndOfLineComment}

Identifier = ([:jletter:] | "_" ) ([:jletterdigit:] | [:jletter:] | "_" )*

/* string and character literals */
StringCharacter = [^\r\n\"\\] /* the final \\ differentiates the special characters */
SingleCharacter = [^\r\n\'\\] /* the final \\ differentiates the special characters */

%state STRING, CHARLITERAL

%%

<YYINITIAL> {
	/* reserved words */ 
	"public"			{ return symbolFactory.newSymbol("PUBLIC", PUBLIC); } 
	"private"			{ return symbolFactory.newSymbol("PRIVATE", PRIVATE); }
	"protected"			{ return symbolFactory.newSymbol("PROTECTED", PROTECTED); }
	"static"			{ return symbolFactory.newSymbol("STATIC", STATIC); }
	"final"				{ return symbolFactory.newSymbol("FINAL", FINAL); }
	"native"			{ return symbolFactory.newSymbol("NATIVE", NATIVE); }
	"synchronized"		{ return symbolFactory.newSymbol("SYNCHRONIZED", SYNCHRONIZED); }
	"abstract"			{ return symbolFactory.newSymbol("ABSTRACT", ABSTRACT); }
	"super"				{ return symbolFactory.newSymbol("SUPER", SUPER); }
	"this"				{ return symbolFactory.newSymbol("THIS", THIS); }
	"new"				{ return symbolFactory.newSymbol("NEW", NEW); }
	"instanceof"		{ return symbolFactory.newSymbol("INSTANCEOF", INSTANCEOF); }
	"return"			{ return symbolFactory.newSymbol("RETURN", RETURN); }
    "throw"			    { return symbolFactory.newSymbol("THROW", THROW); }
    "continue"			{ return symbolFactory.newSymbol("CONTINUE", CONTINUE); }
    "break"             { return symbolFactory.newSymbol("BREAK", BREAK); }
	"try"				{ return symbolFactory.newSymbol("TRY", TRY); }
	"catch"				{ return symbolFactory.newSymbol("CATCH", CATCH); }
	"finally"			{ return symbolFactory.newSymbol("FINALLY", FINALLY); }
	"if"			    { return symbolFactory.newSymbol("IF", IF); }
	"else"			    { return symbolFactory.newSymbol("ELSE", ELSE); }
	"switch"			{ return symbolFactory.newSymbol("SWITCH", SWITCH); }
	"case"				{ return symbolFactory.newSymbol("CASE", CASE); }
	"default"			{ return symbolFactory.newSymbol("DEFAULT", DEFAULT); }
	"while"				{ return symbolFactory.newSymbol("WHILE", WHILE); }
	"do"				{ return symbolFactory.newSymbol("DO", DO); }
	"for"				{ return symbolFactory.newSymbol("FOR", FOR); }
	"package"			{ return symbolFactory.newSymbol("PACKAGE", PACKAGE); }
	"import"			{ return symbolFactory.newSymbol("IMPORT", IMPORT); }
	"class"				{ return symbolFactory.newSymbol("CLASS", CLASS); }
	"interface"			{ return symbolFactory.newSymbol("INTERFACE", INTERFACE); }
	"extends"			{ return symbolFactory.newSymbol("EXTENDS", EXTENDS); }
	"implements"		{ return symbolFactory.newSymbol("IMPLEMENTS", IMPLEMENTS); }
	"boolean"			{ return symbolFactory.newSymbol("BOOLEAN", BOOLEAN); }
	"byte"				{ return symbolFactory.newSymbol("BYTE", BYTE); }
	"char"				{ return symbolFactory.newSymbol("CHAR", CHAR); }
	"short"				{ return symbolFactory.newSymbol("SHORT", SHORT); }
	"int"				{ return symbolFactory.newSymbol("INT", INT); }
	"float"				{ return symbolFactory.newSymbol("FLOAT", FLOAT); }
	"long"				{ return symbolFactory.newSymbol("LONG", LONG); }
	"double"			{ return symbolFactory.newSymbol("DOUBLE", DOUBLE); }
	"void"				{ return symbolFactory.newSymbol("VOID", VOID); }
	
	
	/* literals */
	"null"				{ return symbolFactory.newSymbol("NULL", NULL); }
	"true"				{ return symbolFactory.newSymbol("TRUE", TRUE); }
	"false"				{ return symbolFactory.newSymbol("FALSE", FALSE); }

	/* symbols */
	"{"					{ return symbolFactory.newSymbol("LBRACKET", LBRACKET ); }
	"("					{ return symbolFactory.newSymbol("LPARENTHESIS", LPARENTHESIS); }
	"}"					{ return symbolFactory.newSymbol("RBRACKET", RBRACKET); }
	")"					{ return symbolFactory.newSymbol("RPARENTHESIS", RPARENTHESIS); }
	"["					{ return symbolFactory.newSymbol("LSQUARE_BRACKET", LSQUARE_BRACKET); }
	"]"					{ return symbolFactory.newSymbol("RSQUARE_BRACKET", RSQUARE_BRACKET); }
	","					{ return symbolFactory.newSymbol("COMMA", COMMA); }
	"."					{ return symbolFactory.newSymbol("DOT", DOT); }
	"="					{ return symbolFactory.newSymbol("EQ", EQ); }
	"!"					{ return symbolFactory.newSymbol("NOT", NOT); }
	"?"					{ return symbolFactory.newSymbol("QUESTION", QUESTION); }
	":"					{ return symbolFactory.newSymbol("COLON", COLON); }
	";" 				{ return symbolFactory.newSymbol("SEMICOLON", SEMICOLON); }
	"-"					{ return symbolFactory.newSymbol("MINUS", MINUS); }
	"+"					{ return symbolFactory.newSymbol("PLUS", PLUS); }
	"*"					{ return symbolFactory.newSymbol("MULT", MULT); }
	"/"					{ return symbolFactory.newSymbol("DIVISION", DIVISION); }
	"%"					{ return symbolFactory.newSymbol("MOD", MOD); }
	"++"				{ return symbolFactory.newSymbol("INCREMENT", INCREMENT); }
	"--"				{ return symbolFactory.newSymbol("DECREMENT", DECREMENT); }
	"+="				{ return symbolFactory.newSymbol("PLUS_EQ", PLUS_EQ); }
	"-="				{ return symbolFactory.newSymbol("MINUS_EQ", MINUS_EQ); }
	"*="				{ return symbolFactory.newSymbol("MULT_EQ", MULT_EQ); }
	"/="				{ return symbolFactory.newSymbol("DIVISION_EQ", DIVISION_EQ); }
	"%="				{ return symbolFactory.newSymbol("MOD_EQ", MOD_EQ); }
	
	/* boolean operators */
	">"					{ return symbolFactory.newSymbol("GT", GT); }
	"<"					{ return symbolFactory.newSymbol("LT", LT); }
	"=="				{ return symbolFactory.newSymbol("EQEQ", EQEQ); }
	"<="				{ return symbolFactory.newSymbol("LTEQ", LTEQ); }
	">="				{ return symbolFactory.newSymbol("GTEQ", GTEQ); }
	"!="				{ return symbolFactory.newSymbol("NOTEQ", NOTEQ); }
	"&&"				{ return symbolFactory.newSymbol("AND_AND", AND_AND); }
	"||"				{ return symbolFactory.newSymbol("OR_OR", OR_OR); }
	
	/* logical operators */
	"&"					{ return symbolFactory.newSymbol("AND", AND); }
	"|"					{ return symbolFactory.newSymbol("OR", OR); }
	"^"					{ return symbolFactory.newSymbol("XOR", XOR); }
	"|="				{ return symbolFactory.newSymbol("OR_EQ", OR_EQ); }
	"&="				{ return symbolFactory.newSymbol("AND_EQ", AND_EQ); }
	"^="				{ return symbolFactory.newSymbol("XOR_EQ", XOR_EQ); }
	"||="				{ return symbolFactory.newSymbol("OR_OR_EQ", OR_OR_EQ); }
	
	/* bit operators */
	"~"					{ return symbolFactory.newSymbol("TILDE", TILDE); }
	"<<"				{ return symbolFactory.newSymbol("LSHIFT", LSHIFT); }
	">>"				{ return symbolFactory.newSymbol("RSHIFT", RSHIFT); }
	">>="				{ return symbolFactory.newSymbol("RSHIFT_EQ", RSHIFT_EQ); }
	">>>"				{ return symbolFactory.newSymbol("U_RSHIFT", U_RSHIFT); }
	
	/* string literal */
	\"					{ yybegin(STRING); string.setLength(0); }

	/* char literal*/
	\'					{ yybegin(CHARLITERAL); }
	
	/* rules */
	{Identifier}		{ return symbolFactory.newSymbol("IDENTIFIER", IDENTIFIER, yytext()); }
	{Whitespace}		{ }
	{Comment}			{ }
	{IntegerLiteral}	{ return symbolFactory.newSymbol("INTEGER_LITERAL", INTEGER_LITERAL, new Integer(yytext())); }
	{HexIntegerLiteral}	{ return symbolFactory.newSymbol("HEX_LITERAL", HEX_LITERAL, parseToLong(2, yylength(), yytext(), 16)); }
	{OctIntegerLiteral} { return symbolFactory.newSymbol("OCT_LITERAL", OCT_LITERAL, parseToLong(0, yylength(), yytext(), 8)); }
	{FloatLiteral}		{ return symbolFactory.newSymbol("FLOAT_LITERAL", FLOAT_LITERAL, Float.parseFloat(yytext())); }
}

/*
 The next two sections are necessary to treat special characters,
 including the ones inside a string.
*/
<STRING> {
	\"									{ yybegin(YYINITIAL); return symbolFactory.newSymbol("STRING_LITERAL", STRING_LITERAL, string.toString()); }
	{StringCharacter}+					{ string.append( yytext() ); }
	
	"\\b"								{ string.append('\b'); }
	"\\t"								{ string.append('\t'); }
	"\\n"								{ string.append('\n'); }
	"\\f"								{ string.append('\f'); }
	"\\r"								{ string.append('\r'); }
	"\\\""								{ string.append('\"'); }
	"\\'"								{ string.append('\''); }
	"\\\\"								{ string.append('\\'); }
	\\[0-3]?{OctDigit}?{OctDigit}		{ char val = (char) Integer.parseInt(yytext().substring(1),8); string.append(val); }
}

<CHARLITERAL> {
	{SingleCharacter}\'					{ yybegin(YYINITIAL); return symbolFactory.newSymbol("CHARACTER_LITERAL", CHARACTER_LITERAL, yytext().charAt(0)); }
	
	"\\b"\'								{ yybegin(YYINITIAL); return symbolFactory.newSymbol("CHARACTER_LITERAL", CHARACTER_LITERAL, '\b');}
	"\\t"\'								{ yybegin(YYINITIAL); return symbolFactory.newSymbol("CHARACTER_LITERAL", CHARACTER_LITERAL, '\t');}
	"\\n"\'								{ yybegin(YYINITIAL); return symbolFactory.newSymbol("CHARACTER_LITERAL", CHARACTER_LITERAL, '\n');}
	"\\f"\'								{ yybegin(YYINITIAL); return symbolFactory.newSymbol("CHARACTER_LITERAL", CHARACTER_LITERAL, '\f');}
	"\\r"\'								{ yybegin(YYINITIAL); return symbolFactory.newSymbol("CHARACTER_LITERAL", CHARACTER_LITERAL, '\r');}
	"\\\""\'							{ yybegin(YYINITIAL); return symbolFactory.newSymbol("CHARACTER_LITERAL", CHARACTER_LITERAL, '\"');}
	"\\'"\'								{ yybegin(YYINITIAL); return symbolFactory.newSymbol("CHARACTER_LITERAL", CHARACTER_LITERAL, '\'');}
	"\\\\"\'							{ yybegin(YYINITIAL); return symbolFactory.newSymbol("CHARACTER_LITERAL", CHARACTER_LITERAL, '\\'); }
	\\[0-3]?{OctDigit}?{OctDigit}\'		{ yybegin(YYINITIAL); int val = Integer.parseInt(yytext().substring(1,yylength()-1),8);
			                            			return symbolFactory.newSymbol("CHARACTER_LITERAL", CHARACTER_LITERAL, (char)val); }
}

// error fallback
.|\n			{ emit_warning("Unrecognized character '" +yytext()+"' -- ignored"); }
<<EOF>>			{ return symbolFactory.newSymbol("EOF",sym.EOF); }
