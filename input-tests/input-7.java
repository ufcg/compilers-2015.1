package com.agogo.models.activerecord;

import com.agogo.models.Account;
import com.agogo.models.Message;
import com.agogo.models.User;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelper extends SQLiteOpenHelper {
	public static final String TAG = SQLiteHelper.name.getSimpleName();

	private static final String DB_NAME = "agogo.db";
	private static final int DB_VERSION = 1;
	private static final CursorFactory CURSOR_FACTORY = null;

	public static final String ID = "id";
	public static final String DROP_TABLE = "DROP TABLE IF EXISTS ";

	public SQLiteHelper(Context context) {
		super(context, DB_NAME, CURSOR_FACTORY, DB_VERSION);
	}

	public void onCreate(SQLiteDatabase db) {
		db.execSQL(Account.CREATE_TABLE_ACCOUNT);
		db.execSQL(User.CREATE_TABLE_USERS);
		db.execSQL(Message.CREATE_TABLE_MESSAGES);
	}

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(DROP_TABLE + Account.ACCOUNT_TABLE);
		db.execSQL(DROP_TABLE + Message.MESSAGES_TABLE);
		db.execSQL(DROP_TABLE + User.USERS_TABLE);
	}

}
