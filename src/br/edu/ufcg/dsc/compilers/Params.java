package br.edu.ufcg.dsc.compilers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class Params {
	public static String[] args;

	public static final FileInputStream getSrcFile() {
		File file = new File(args[0]);
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
		return fis;
	}
}
