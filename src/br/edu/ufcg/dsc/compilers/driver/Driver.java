package br.edu.ufcg.dsc.compilers.driver;

import br.edu.ufcg.dsc.compilers.Params;
import br.edu.ufcg.dsc.compilers.gen.Parser;

class Driver {

	public static void main(String[] args) {
		Params.args = args;

		Parser parser = new Parser();
		
		try {
			parser.parse();
			System.out.println("Compiled with success!");
		} catch (Throwable e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
}